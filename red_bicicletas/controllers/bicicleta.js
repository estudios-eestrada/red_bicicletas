// Importar Módelo Bicicleta
var Bicicleta = require('../models/bicicleta');

// Generar (Petición De) Listado De Bicicletas
exports.bicicleta_List = function(req,res) {

  res.render('bicicletas/index', {bicis: Bicicleta.allBicis});

}

//  Petición Crear Bicicleta GET Y POST
exports.bicicleta_create_get = function(req,res) {

  res.render('bicicletas/create');

}

exports.bicicleta_create_post = function(req,res) {

  var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);

  bici.ubicacion = [req.body.lat, req.body.lng];
  Bicicleta.add(bici);

  res.redirect('/bicicletas');

}

// Petición Eliminar Bicicleta POST
exports.bicicleta_delete_post = function(req,res) {

  Bicicleta.removeById(req.body.id);

  res.redirect('/bicicletas');

}

//  Petición Modificar Bicicleta GET Y POST
exports.bicicleta_update_get = function(req,res) {

  var bici = Bicicleta.findById(req.params.id);

  res.render('bicicletas/update', {bici});

}

exports.bicicleta_update_post = function(req,res) {

  var bici = Bicicleta.findById(req.params.id);

  bici.id = req.body.id;
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.lat, req.body.lng];

  res.redirect('/bicicletas');

}
