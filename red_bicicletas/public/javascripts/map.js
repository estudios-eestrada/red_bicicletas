var map = L.map('mapid').setView([14.600278,  -90.524890], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

//L.marker([14.597764,-90.524755]).addTo(map);
//L.marker([14.596891,-90.521688]).addTo(map);
//L.marker([14.596331,-90.522188]).addTo(map);

// Llamada Ajax PARA API Rest
$.ajax({

  dataType: "json",
  url: "api/bicicletas",
  success: function(result){

    console.log(result);
    result.bicicletas.forEach(function(bici){

      L.marker(bici.ubicacion, {title:bici.id}).addTo(map);

    });
  }
})
