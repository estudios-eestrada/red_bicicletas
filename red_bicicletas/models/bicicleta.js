// Crear Constructor De Modelo(Objeto) Bicicleta
var Bicicleta = function(id, color, modelo, ubicacion) {

  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;

}

// Prototipo String
Bicicleta.prototype.toString = function() {

  return 'id' + this.id + " | Color: " + this.color;

}

// Array Para Listar Bicicletas
Bicicleta.allBicis = [];

// Método Para Agregar Bicicleta A Lista
Bicicleta.add = function(aBici){

  Bicicleta.allBicis.push(aBici);
}

// Método Para Buscar Bicicleta Por ID
Bicicleta.findById = function(aBiciId) {

  var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
  if(aBici)
    return aBici;
  else
    throw new Error(`No Existe Una Bicicleta Con El Id ${aBiciId}`);

}

// Método Para Remover Bicicleta De Lista
Bicicleta.removeById = function(aBiciId) {

  // Buscar Bici Y Eliminar
  for(var i=0; i<Bicicleta.allBicis.length; i++) {

    if(Bicicleta.allBicis[i].id == aBiciId) {

      Bicicleta.allBicis.splice(1,1);
      break;
    }
  }
}

// Crear Primeras 2 Bicicletas
//var a = new Bicicleta(1, 'Rojo', 'Urbana', [14.597764,-90.524755]);
//var b = new Bicicleta(2, 'Blanco', 'Urbana', [14.596891,-90.521688]);
//Bicicleta.add(a);
//Bicicleta.add(b);

// Exportar Módulo(Objeto)
module.exports = Bicicleta;
