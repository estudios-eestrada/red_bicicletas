// TESTING MODELO BICICLETA
// Agregar Modelo
var Bicicleta = require('../../models/bicicleta');

// ----- PRUEBAS ----- //

// Hacer Antes De Cada Prueba
beforeEach( () => { Bicicleta.allBicis = [] });

// Probar Si Comienza Vacia
describe('Bicicleta.allBicis', () => {

  it('Comienza Vacia', () => {

    expect(Bicicleta.allBicis.length).toBe(0);

  });
});

// Prueba Si Comienza Vacia, Se Agrega Bicicleta, Verificar Que Agregue
describe('Bicicleta.add', () => {

  it('Agregar Una', () => {

    expect(Bicicleta.allBicis.length).toBe(0);

    var a = new Bicicleta(2, 'Blanco', 'Urbana', [14.596891,-90.521688]);
    Bicicleta.add(a);

    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);

  });

});

// Prueba Busqueda finById De Modelo
describe('Bicicleta.findById', () => {

  it('Devolver Bici Con Id 1', () => {

    expect(Bicicleta.allBicis.length).toBe(0);

    var aBici = new Bicicleta(1, 'Blanco', 'Urbana', [14.596891,-90.521688]);
    var aBici2 = new Bicicleta(2, 'Verde', 'Urbana', [14.596000,-90.521600]);
    Bicicleta.add(aBici);
    Bicicleta.add(aBici2);

    var targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(aBici.color);
    expect(targetBici.modelo).toBe(aBici.modelo);

  });
});

// Prueba Eliminar emoveById De Módulo
describe('Bicicleta.removeById', () => {

  it('Devolver Ultimo Elemento Despues De Borrar Ultimo', () => {

    // Verificar Que No Hayan Bicicletas Registradas
    expect(Bicicleta.allBicis.length).toBe(0);

    // Agregar 3 Bicicletas De Prueba
    var aBici = new Bicicleta(1, 'Blanco', 'Urbana', [14.596891,-90.521688]);
    var aBici2 = new Bicicleta(2, 'Verde', 'Urbana', [14.596000,-90.521600]);
    var aBici3 = new Bicicleta(3, 'morado', 'Clasica', [14.596000,-90.521600]);
    Bicicleta.add(aBici);
    Bicicleta.add(aBici3);
    Bicicleta.add(aBici2);

    // Remover Una Bicicleta
    Bicicleta.removeById(2);

    // Verificar Que Se Haya Removida la Bicicleta
    expect(Bicicleta.allBicis.length).toBe(2);

  });
});
